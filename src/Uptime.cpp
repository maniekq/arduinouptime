#include "Uptime.h"
#include <Arduino.h>

void Uptime::loop() {

  long now = millis();
  long msSinceLastLoop = now - lastRunMillis;

  if (msSinceLastLoop < REFRESH_PERIOD_MS) {
    return;
  }

  if (lastRunMillis > now) {
    rollovers++;
  }

  long secsUp = millis() / 1000;
  seconds = secsUp % 60;
  minutes = (secsUp / 60) % 60;
  hours = (secsUp / (60 * 60)) % 24;
  days = (rollovers * 50) + (secsUp / (60 * 60 * 24));
};

long Uptime::getDays() {
  return days;
}

int Uptime::getHours() {
  return hours;
}

int Uptime::getMinutes() {
  return minutes;
}

int Uptime::getSeconds() {
  return seconds;
}

const char* Uptime::toString() {
  String uptimeString = "P";
  uptimeString += days;
  uptimeString += "DT";
  uptimeString += hours;
  uptimeString += 'H';
  uptimeString += minutes;
  uptimeString += 'M';
  uptimeString += seconds;
  uptimeString += 'S';

  char* returnedUptimeString = new char[uptimeString.length()+1];
  uptimeString.toCharArray(returnedUptimeString, uptimeString.length()+1);
  return returnedUptimeString;
}

#ifndef _UPTIME_H_
#define _UPTIME_H_

class Uptime {
public:
  void loop();
  long getDays();
  int getHours();
  int getMinutes();
  int getSeconds();
  const char* toString();
private:
  long days = 0;
  int hours = 0;
  int minutes = 0;
  int seconds = 0;
  int highMillis = 0;
  int rollovers = 0;
  long lastRunMillis = 0;
  const long REFRESH_PERIOD_MS = 30000; // refresh uptime every 30 seconds
};

#endif // _UPTIME_H_